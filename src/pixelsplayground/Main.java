package pixelsplayground;

import java.io.IOException;

public class Main {

    public static final int WIDTH = 3840;
    static final int HEIGHT = 2160;
    static final float[] DOMAIN = {2.0f, 2.0f};
    static final float[] CENTER = {0, 0};

    public static void main(String[] args) throws IOException {
//        double[] randomCenter = Auxiliary.randomMisiurewiczPoint();

//        Auxiliary.write("test.jpg",
//                Auxiliary.colorizeHSB(
//                        Fractal.test(WIDTH, HEIGHT, DOMAIN, CENTER)));

//        Auxiliary.write("mandelbrot.jpg",
//                Auxiliary.colorizeHSB(
//                        Fractal.mandelbrot(WIDTH, HEIGHT, DOMAIN, CENTER)));

//        Auxiliary.write("mandelbrotSmoothInterpolation.jpg",
//                Auxiliary.colorizeHSB(
//                        Fractal.mandelbrotSmoothInterpolation(WIDTH, HEIGHT, DOMAIN, CENTER)));

        Auxiliary.write("buddhabrot 100 passes.jpg",
                Auxiliary.colorize(WIDTH,HEIGHT,
                        Fractal.mandelbrot(WIDTH, HEIGHT, DOMAIN, CENTER)));

    }
}
