package pixelsplayground;

public interface IFractal {

    static int[] buddhabrot(int w, int h, float[] area, float[] center) {
        int[] pixels = new int[w * h];
        return pixels;
    }

    static int[] mandelbrot(int w, int h, float[] area, float[] center) {
        int[] pixels = new int[w * h];
        return pixels;
    }

    static int[] mandelbrotSmoothInterpolation(int w, int h, float[] area, float[] center) {
        int[] pixels = new int[w * h];
        return pixels;
    }

    static int[] test(int[] WH, float[] domain, float[] center) {
        int[] pixels = new int[WH[0] * WH[1]];
        return pixels;
    }

}
