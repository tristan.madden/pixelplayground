package pixelsplayground;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.image.ColorModel;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.awt.Color;

class Auxiliary {

    static float mapFloat(float value, float istart, float istop, float ostart, float ostop) {
        return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    }

    static double mapDouble(double value, double istart, double istop, double ostart, double ostop) {
        return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    }

    static double[] randomMisiurewiczPoint() {
        double[][] points = {{-0.7756, 0.1364}, {-0.1010, 0.9562}};
        Random rand = new Random();
        int randomNum = rand.nextInt((points.length - 0) + 0) + 0;
        return points[randomNum];
    }

    static int raw(int iter) {
        //int argb = (iter << 24) | (iter << 16) | (iter << 8) | iter;
        //System.out.println(iter);
        int argb = new Color(255 - iter, 255 - iter, 255 - iter).getRGB();
        //System.out.println(argb);
        return argb;
    }

    static int monochrome(int iter, int max_iter) {
        double m = Math.sqrt((double) iter / (double) max_iter);
        int b = (int) (Math.sin(m) * 255);
        int argb = ((b << 24) | (b << 16) | (b << 8) | b);
        return argb;
    }

    static int color(int iter, int max_iter) {
        double m = Math.sqrt((double) iter / (double) max_iter);
        int a = 0xFF;
        int r = (int) (Math.sin(m * 2) * 255);
        int g = (int) (Math.sin(m * 4) * 255);
        int b = (int) (Math.sin(m * 6) * 255);
        int argb = ((a << 24) | (r << 16) | (g << 8) | b);
        return argb;
    }

    static int[] colorize(int w, int h, int[] pixels) {

        for (int i = 0; i < pixels.length; i++) {
            int r = (int) Math.sin(pixels[i]) * 255;
            int g = (int) Math.cos(pixels[i]) * 255;
            int b = pixels[i];
            if (r > 255) {
                r = 255;
            } else if (r < 0) {
                r = 0;
            }
            if (g > 255) {
                g = 255;
            } else if (g < 0) {
                g = 0;
            }
            if (b > 255) {
                b = 255;
            } else if (b < 0) {
                b = 0;
            }
//            System.out.println("r: " + r + "g: " + g + "b: " + b);
            Color myRGBColor = new Color(r, g, b);
            pixels[i] = myRGBColor.getRGB();
        }
        return pixels;
    }

    static int[] colorizeHSB(int[] pixels) {
        
        int maxexposure = 0;
        // find the largest density value
        for (int i = 0; i < pixels.length; i++) {
            maxexposure = Math.max(maxexposure, pixels[i]);
        }
        // draw to screen
        for (int x = 0; x < Main.WIDTH; x++) {
            for (int y = 0; y < Main.HEIGHT; y++) {
                
                float p = pixels[y * Main.WIDTH + x];
                float ramp = p / (maxexposure / 3.0f);
                //float m = Auxiliary.mapFloat(p, 0, maxexposure, 0.5f, 0.70f);

                // blow out ultra bright regions
                if (ramp > 1) {
                    ramp = 1;
                }
                
                Color myRGBColor = Color.getHSBColor(ramp*0.25f, ramp, ramp);
                pixels[y * Main.WIDTH + x] = myRGBColor.getRGB();
                
            }
        }

        return pixels;
    }
    
//    static int smoothColor(int iter, int max_iter) {
//        double m = Math.sqrt((double) iter / (double) max_iter);
//        int a = 0xFF;
//        int r = (int) (Math.sin(m * 1) * 255);
//        int g = (int) (Math.sin(m * 2) * 255);
//        int b = (int) (Math.sin(m * 3) * 255);
//        int argb = ((a << 24) | (r << 16) | (g << 8) | b);
//        return argb;
//    }

    static void write(String name, int[] data) throws IOException {

        System.out.println(" - - - Writing To File - - - ");
        DataBufferInt buffer = new DataBufferInt(data, data.length);
        int[] bandMasks = {0xFF0000, 0xFF00, 0xFF, 0xFF000000};
        WritableRaster raster = Raster.createPackedRaster(buffer, Main.WIDTH, Main.HEIGHT, Main.WIDTH, bandMasks, null);
//        System.out.println("raster: " + raster);
        ColorModel cm = ColorModel.getRGBdefault();
        BufferedImage image = new BufferedImage(cm, raster, cm.isAlphaPremultiplied(), null);
        System.err.println("image: " + image);

        File outputfile = new File("output//" + name);
        ImageIO.write(image, "png", outputfile);
    }
}
