package pixelsplayground;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

class Fractal implements IFractal {

    private static DecimalFormat df2 = new DecimalFormat("##.##");

    static int[] buddhabrot(int w, int h, float[] domain, float[] center) {
        
        System.out.println(" - - - Rendering - - - ");

        int[] pixels = new int[w * h];

        for (int i = 0; i < 200; i++) {

            float progress = Auxiliary.mapFloat(i, 0, 100, 0, 100);

            System.out.println(df2.format(progress) + "% finished rendering.");
            
            plot(w, h, domain, center, pixels);

        }

        return pixels;
    }

    static void plot(int w, int h, float[] domain, float[] center, int[] pixels) {

        Random random = new Random();

        for (int x = 0; x < w; x++) {
            
            for (int y = 0; y < h; y++) {

                double x0 = Auxiliary.mapFloat(x, 0, w, center[0] + (-domain[0]), center[0] + domain[0]);
                double y0 = Auxiliary.mapFloat(y, 0, h, center[1] + (-domain[1]), center[1] + domain[1]);
                double xd = random.nextGaussian();
                double yd = random.nextGaussian();

                x0 += xd;
                y0 += yd;

                if (iterate((float) x0, (float) y0, w, h, false, pixels)) {
                    iterate((float) x0, (float) y0, w, h, true, pixels);
                }

            }
        }

    }

    //Iterate the Mandelbrot and return TRUE if the point escapes
    //Also handle the drawing of the exit points
    static private boolean iterate(float x0, float y0, int w, int h, boolean drawIt, int[] exposure) {
        int BAILOUT = 777;
        float x = 0;
        float y = 0;
        float xnew, ynew;

        float aspectRatio = (float) w / (float) h;
        float scaledX1 = -2.0f * aspectRatio;
        float scaledX2 = 2.0f * aspectRatio;
        float scaledY1 = -2.0f;
        float scaledY2 = 2.0f;

        for (int i = 0; i < BAILOUT; i++) {

            ynew = (x * x - y * y) + x0;
            xnew = (2 * x * y) + y0;

            if (drawIt && (i > 3)) {

                int ix = (int) Auxiliary.mapFloat(xnew, scaledX1, scaledX2, 0, w);
                int iy = (int) Auxiliary.mapFloat(ynew, scaledY1, scaledY2, 0, h);

                if (ix >= 0 && iy >= 0 && iy < h && ix < w) {
                    // rotate and expose point
                    exposure[iy * w + ix]++;
                }
            }
            if ((xnew * xnew + ynew * ynew) > 4) {
                // escapes
                return true;
            }
            y = xnew;
            x = ynew;
        }
        //does not escape
        return false;
    }

    static int[] mandelbrot(int w, int h, float[] area, float[] center) {
        System.out.println(" - - - Rendering - - - ");

        int[] pixels = new int[(int) w * (int) h];
        float[] areaXY = Arrays.copyOf(area, 2);
        int MAX_ITER = 255;
        float zx, zy, cx, cy, temp;
        float aspect = (float) w / (float) h;

        if (aspect < 1) {
            areaXY[1] /= aspect;
        } else {
            areaXY[0] *= aspect;
        }

        for (int index = 0, y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                zx = zy = 0;
                cx = Auxiliary.mapFloat(x, 0, w, center[0] + (-areaXY[0]), center[0] + areaXY[0]);
                //cx = (x - w / 2.0f) * (4.0f) / w;
                //cy = Auxiliary.mapFloat(y, 0, h, center[1] + (-areaXY[1]), center[1] + areaXY[1]);
                cy = (y - h / 2.0f) * (4.0f) / h;

                int iter = 0;
                while (zx * zx + zy * zy < 4 && iter++ < MAX_ITER) {
                    temp = zx * zx - zy * zy + cx;
                    zy = 2 * zx * zy + cy;
                    zx = temp;
                }
                if (iter < MAX_ITER) {
                    int argb = Auxiliary.color(iter, MAX_ITER);
                    pixels[index] = argb;
                }
                index++;
            }
        }
        return pixels;
    }

    static int[] mandelbrotSmoothInterpolation(int w, int h, float[] area, float[] center) {
        System.out.println(" - - - Rendering - - - ");

        float[] areaXY = Arrays.copyOf(area, 2);
        final float r = 10; //escape radius
        final float r2 = 100; //r-squared
        int[] pixels = new int[(int) w * (int) h];
        int MAX_ITER = 255;
        float zx, zy, cx, cy, temp;
        float aspect = w / h;

        if (aspect < 1) {
            areaXY[1] /= aspect;
        } else {
            areaXY[0] *= aspect;
        }

        for (int index = 0, y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                zx = zy = 0;
                cx = Auxiliary.mapFloat(x, 0, w, center[0] + (-areaXY[0]), center[0] + areaXY[0]);
                cy = Auxiliary.mapFloat(y, 0, h, center[1] + (-areaXY[1]), center[1] + areaXY[1]);
                int iter = 0;

                while (zx * zx + zy * zy < r2 && ++iter < MAX_ITER) {
                    temp = zx * zx - zy * zy + cx;
                    zy = 2 * zx * zy + cy;
                    zx = temp;
                }

                float dist = zx * zx + zy * zy;
                float fracIter = (dist - r) / (r2 - r); //remap into the 0-1 range

//                int argb = Auxiliary.monochrome((int)fracIter*255, MAX_ITER);
                int argb = Auxiliary.raw(iter);
                pixels[index++] = argb;
            }
        }
        return pixels;
    }

    static int[] test(int w, int h, float[] area, float[] center) {

        int[] pixels = new int[w * h];

        for (int index = 0, y = 0; y < h; y++) {

            for (int x = 0; x < w; x++) {
                if (x % 2 == 0) {
                    pixels[index++] = 255 % (y + 1);
                } else {
                    pixels[index++] = 255 % (x + 1);
                }

            }
        }

        return pixels;
    }

}
